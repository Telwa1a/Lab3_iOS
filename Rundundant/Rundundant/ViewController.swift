//
//  ViewController.swift
//  Rundundant
//
//  Created by DevComputer on 2017-11-12.
//  Copyright © 2017 BrokenDevelopment. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import AVFoundation

class LocationManager {
    static let shared = CLLocationManager()
    private init () {}
}

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var runTime: UILabel!
    @IBOutlet weak var runDist: UILabel!
    @IBOutlet weak var runMapView: MKMapView!
    
    private var runDistance = Measurement(value: 0, unit: UnitLength.meters)
    private var audioPlayer : AVAudioPlayer!
    private var soundPlayed = 1
    
    var previousLocation : CLLocation?    
    var locMag : CLLocationManager!
    //private let locationManager = LocationManager.shared
    //private var locationList: [CLLocation] = []
    
    func playSound(){
        guard let url = Bundle.main.url(forResource: "applause2", withExtension: "mp3") else { return }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            audioPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            guard let audioPlayer = audioPlayer else { return }
            audioPlayer.play()
        } catch let error {
            print (error.localizedDescription)
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //Initialize map view
        runMapView.delegate = self
        runMapView.showsUserLocation = true
        runMapView.mapType = MKMapType(rawValue: 0)!
        runMapView.userTrackingMode = MKUserTrackingMode(rawValue: 2)!
        
        //Initialize custom LocationManager
        locMag = CLLocationManager()
        locMag.desiredAccuracy = kCLLocationAccuracyBest
        locMag.delegate = self
        
        let status = CLLocationManager.authorizationStatus()
        if (status == .notDetermined || status == .denied || status == .authorizedWhenInUse) {
            locMag.requestAlwaysAuthorization()
            locMag.requestWhenInUseAuthorization()
        }
        
        locMag.startUpdatingLocation()
        locMag.startUpdatingHeading()
        
        //Initialize certain values
        var runTimeInSeconds = 0
        
        //Fix run time
        _ = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) {_ in
            runTimeInSeconds += 1
            
            if UIApplication.shared.applicationState == .active
            {
                self.runTime.text = String(runTimeInSeconds) + " seconds"
                self.runDist.text = String(describing: self.runDistance) + "eters"
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation = locations[0]
        
        //Make sure to only calculate reasonable location deltas
        if let currentDistance = previousLocation?.distance(from: currentLocation)
        {
            //MAXIMUM DISTANCE AT EACH UPDATE IS 1 KILOMETER
            if !(currentDistance > 1000)
            {
                runDistance.value += Double(currentDistance)
            }
        }
        
        //Define route line on map
        var routeLine : MKPolyline
        
        //Define where route line should be drawn and at which points
        if (previousLocation != nil)
        {
            let prevLocation : CLLocation = previousLocation!
            var linePoints = [currentLocation.coordinate, prevLocation.coordinate]
            routeLine = MKPolyline(coordinates: &linePoints, count: linePoints.count)
        }
        else
        {
            var linePoints = [currentLocation.coordinate, currentLocation.coordinate]
            routeLine = MKPolyline(coordinates: &linePoints, count: linePoints.count)
        }
        
        previousLocation = currentLocation
        runMapView.add(routeLine)
        
        //Play sound effect for each 1000 meter reached
        let playTime = 1000.0 * Double(soundPlayed)
        
        if (self.runDistance.value > playTime) {
            self.playSound()
            self.soundPlayed += 1
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 3
        
        return renderer
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
